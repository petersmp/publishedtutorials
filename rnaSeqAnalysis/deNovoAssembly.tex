%% Chapter on QC and processing

\chapter{De Novo Assembly}
\label{chap:deNovoAssembly}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

One of the most computationally difficult processes in RNA-seq
is aligning all of the reads together into transcripts.
%
This can be eased by aligning them against a reference genome or transcriptome,
but sometimes references are not available.
%
Only a small number of eukaryotes,
and even fewer animals,
have had their genomes sequenced.
%
This means that analyzing any non-model system
will often require assembling the sequences
with just themselves (\textit{de novo}).


This process is a major computational task,
often requiring days or even weeks to run.
%
Quite simply:
this is not something you want to run on your local machine.
%
Even many online tools don't
allow users to run assembly due to the computational cost.


There are a growing number of assembly programs,
and each has unique benefits and drawbacks.
%
Below,
we will just touch on some of the basics of these assemblers
and set up a de novo assembly to run with the cleaned reads
that everyone has worked with.
%
We will likely not cover this section in the workshop,
but it is here for your edification and benefit.


This module is going to focus on running
a simple RNA assembly from the command line,
%
We will walk through each step in the process,
leading to a single job with input from all of the samples.
%
The focus in these written instructions will be on running the program,
rather than on what the program is doing.
%
For further information on what each assembler does,
why you might choose a particular assembler, and
what parameters may need tweaking,
please see the documentation for these projects,
and the links below
(see section \ref{sect:AssemFurther}).

\begin{figure}[h]
\begin{center}
% \includegraphics[width=.45\textwidth]{../images/shenemangenome}
\caption{Drew Sheneman -- The Newark Star Ledger; available online at
	\href{http://www.genomicglossaries.com/presentation/SLAgenomics.asp}{genomicglossaries.com/presentation/SLAgenomics.asp}}
%\href{http://xkcd.com/1168}{xkcd.com/1168}}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
 \item Learn the basics of starting an RNA assembly
 \item Understand the basics of Linux command line scripting
 \item Understand the basics of what Trinity,
	or a similar short read assembler, does
\end{itemize}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Vision and Change Competencies addressed}  %%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{itemize}
%  \item Ability to apply the process of science
%  \item Ability to use quantitative reasoning
%  \item Ability to use modeling and simulation
% \end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Getting the data together} %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For this assembly, we will need to get all of the sequence data 
combined into a single (very large) file.
%
%
For that, we will use the \verb+cat+ (concatenate) command,
and create a new file for each the left and right reads.
%
The code below will combine the listed files together.
%
Replace the dots and the last file with the remaining files to concatenate,
and this will create a single file for all of the reads.
%
If you trimmed your sequence data,
you should use the trimmed versions here.
%
Low quality sequence data is a much larger hindrance to assembly
than it is to gene expression analysis.

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Do NOT run this:}}
cd \dataDirectory{}/seqData
cat \hlnum{fileNameA} \hlnum{fileNameB} \hlnum{fileNameC} > allSamples.fq
\end{alltt}\end{kframe}\end{knitrout}


This command combines all of the named files together,
and then writes them to a new file.
%
Be careful with the single ``\verb;>;''
as it will overwrite a file of the same name.
Using ``\verb;>>;'' will append the new information to an existing file,
but can cause problems with accidentally duplicating information in a file unless care is taken.
%
That was a lot of typing, and introduces several opportunities for error.
%
The problem only gets worse with more files
(these sample data came from a set with 90 samples).
%
Luckily there is a \sout{lazier} more efficient and safer solution
using the wild card character (*):

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Only one student should run this:}}
cd \dataDirectory{}/seqData
cat *.fq  > allSamples.fq
\end{alltt}\end{kframe}\end{knitrout}

Which will match any of the files
ending with ``.fq'' and put them together in a single script.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Set up the assembly script} %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Copy one of your old \verb+qsub+ scripts,
then open it with nano
(or, edit it on your machine, and then \verb+scp+ it up).
%
Set the required memory to 10 GB (\verb+vmem=10gb+),
and the time to 6 hours (\verb+walltime=6:00:00 +).
%
If you are doing this for your own data,
you will want much larger memory  (e.g. 300gb)
and time (e.g. 144 hours).
%
Yes, assembly really can take that long;
sometimes even longer,
especially if you have more sequence data.
%
This is why you need dedicated computing resources to run this,
unless you have a desktop PC with 300 GB of RAM
that you don't need for the next week.


Now, we need to add the programs that we will be running.
%
Luckily for us,
all of the programs we need
(Trinity, samtools, java, and bowtie)
are already available on many systems
(including Mason).
%
A current list of software available
% on Mason is kept at
% \url{ncgas.org/software.php},
% or
can be found by running the command
``\verb;module avail;''.
%
% Because the system knows that samtools, java, and bowtie
% are required by Trinity
% (they are ``dependencies'')
% the system will automatically load them
% when we tell it to load Trinity.
%
To load these programs before running Trinity,
add the following lines to your script:

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Put this in your new script file}}
module load java
module load samtools/1.2
module load trinityrnaseq
\end{alltt}\end{kframe}\end{knitrout}


Note that you can also run this from the command line,
if you want to use Trinity directly from the command line;
though the time limits mean you are unlikely to be able to
accomplish anything.
%
If you want to load Trinity (or any other module)
every time you log on to the computer system,
you can add the above line to the file
``.modules'' in your home directory.


Next, set the working directory for the script to the
directory holding our sequence data.

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Put this in your new script file}}
cd \dataDirectory{}/seqData
\end{alltt}\end{kframe}\end{knitrout}


We are now ready to add the call to Trinity itself to the script.
There are many, many options for Trinity,
but we will stick with the simplest of them for this assembly.
%
For more details on options,
look at the web documentation for Trinity linked below
(section \ref{sect:AssemFurther}). 
%
Trinity is a Perl script,
which means that it calls another programming language
(which you don't need to learn)
in order to execute the commands.
%
The available options, and a usage example,
can be found by calling ``\verb;Trinity --help | less;''
%
(note that this ``pipes'' the help through \verb;less; to make it easier to scroll).
%
Below are some very basic approaches,
which should give you the idea.
%
Add this to your script:

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Put this in your new script file}}
Trinity.pl --seqType fq --max_memory 10G --CPU 1 \textbackslash
           --output GCAT_Trinity --single allSamples.fq
\end{alltt}\end{kframe}\end{knitrout}

These commands call Trinity.
%
It sets the ``\verb;seqType;'' to fastq format.
%
Allows the computer to use up to 10 GB of RAM
(also needs to be set at the top of the script,
and should be changed for your actual data) on one CPU.
%
It will create a new directory named ``GCAT\_Trinity'' for the output,
and will use the sequence file we created just above.
%
This one line will run the full assembly for us.
%
Finally, as before,
(though here \emph{only one} student will submit the job)
set the job to run with \verb+qsub+,
and wait.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Assembly is one of the largest challenges in bioinformatics right now.
%
Fortunately, many new approaches are emerging,
based on both theoretical and practical exploration.
%
Unfortunately,
that means that by the time you get around to looking for help,
everything will have changed since I wrote this.
%
Using this module as a starting point will likely get you very far,
as Trinity is one of the better assemblers at the moment.
%
If (read: when) you run into problems,
ask around to see if it is a common problem,
and test out your Google-fu to see if others have already solved it.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sect:AssemFurther}

More information related to this topic can be found at:
\begin{itemize}
 \item \url{trinityrnaseq.sourceforge.net/}
\end{itemize}


