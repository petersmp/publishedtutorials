%% Chapter on QC and processing

\chapter{Read Mapping}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RNA-seq read mapping is the process of aligning reads against a genome
(or transcriptome) in order to
identify the position from which the RNA originated.
%
This is another computationally intensive task,
as it is generally attempting to align millions of reads
against thousands of genes (or a full genome).
%
In the case of alignment to a genome,
the problem is further complicated by the fact
that the sequenced RNA contains only exons,
while in the genome these exons are substantially split by large introns.
%
Even against transcriptomes,
alternative splicing, skipped exons,
and a number of processing steps can complicate the process.


Thus, many of the standard alignment tools
that were developed for pairwise sequence alignment
(e.g. Blast)
are not suitable for this task.
%
However, many alignment tools are emerging specifically designed
to handle RNA alignment to genomes or transcriptomes.
%
These tools use much shorter alignment matches
and are designed to detect splicing and gene graphs.
%
They work on similar principles to traditional alignment tools,
and we will discuss them more as a group.

\begin{figure}[h]
\begin{center}
\includegraphics[width=.4\textwidth]{../images/xkcd_abstraction_676}
\caption{\href{http://xkcd.com/1168}{xkcd.com/676}
	 (Creative Commons Licencse)
}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
 \item Learn about RNA alignment
 \item Learn the basic premise of different read mapping tools
 \item Understand the basic principles of RNA mapping
 \item Understand how to utilize various mapping tools
\end{itemize}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Vision and Change Competencies addressed}  %%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{itemize}
%  \item Ability to apply the process of science
%  \item Ability to use quantitative reasoning
%  \item Ability to use modeling and simulation
% \end{itemize}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Choosing an aligner}  %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Possibly for the intro???
% For this chapter, 
% we will use command line linux to align the
% processed samples against a transcriptome.
% %
% This chapter is where we are (finally) beginning to create the
% final datasets that will allow us to generate some answers.
% %
% Many of the mapping processes described are very memory intensive
% , and, the best approach is often to submit them to a command line computer cluster.


There are several mapping tools available,
and many are being developed specifically with short,
and/or paired RNA reads in mind,
such as bowtie and tophat from the tuxedo package.
%
Bowtie
is a fast read aligner,
though it does not handle splicing explicitly.
%
For aligning to a genome,
Tophat uses initial bowtie reads to predict splice sites,
and then aligns more reads across those junctions.
%
Alternatively, if the transcripts are already known
(through either transcriptome sequencing,
like with our sample data,
or gene prediction),
bowtie can be used to align directly against known splice forms.


In this chapter,
we will use a program called RSEM (RNA-Seq by Expectation-Maximization)
that uses the bowtie alignment of reads to known transcripts.
%
The program offers alternatives,
including using the output from any aligner instead of using bowtie,
that are discussed more thoroughly in the documentation linked below.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Installing RSEM}  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The RSEM package is a simple one to install,
but requires several other programs (dependencies) to run completely.
%
This installation will be broken into two parts,
installing RSEM and installing those dependencies.
%
First, move the directory in which you would like to install
(\textasciitilde/opt),
then follow these commands,
which may be starting to look familiar now.
%
The addition of the ``\verb;make;'' command is necessary
because RSEM needs to be compiled for the system's architecture.
%
After running make, we add the path to our environment.

\begin{knitrout}\begin{kframe}\begin{alltt}
cd \mytilde/opt
wget https://github.com/deweylab/RSEM/archive/v1.2.30.tar.gz
tar -zxvf v1.2.30.tar.gz
rm -i v1.2.30.tar.gz 

cd RSEM-1.2.30/
make

export PATH=\$PATH:\$HOME/opt/RSEM-1.2.30
echo 'export PATH=\$PATH:\$HOME/opt/RSEM-1.2.30'  >>  \mytilde/.bashrc
\end{alltt}\end{kframe}\end{knitrout}


Next, we  can add bowtie to our environment
using a ``module''.
%
This is a method that is slightly different than we used before, and
its specific implementation will depend on the system you are working in.
%
The basic idea is that updates to shared software
(software available to all users)
will be implemented more seamlessly this way.
%
It is not available on all systems,
but is a nice feature of some computer systems,
as it will automatically load the program,
and add it to our path,
for us.
%
As before, we are going to add the command to a profile
to ensure that they are loaded every time instead of
manually loading each time we sign in (or run a script).
%

\begin{knitrout}\begin{kframe}\begin{alltt}
module load bowtie
echo 'module load bowtie' >> \mytilde/.modules
\end{alltt}\end{kframe}\end{knitrout}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Prepare for alignment} %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We now have all of the software we need set to go,
and can begin working on the process that
will run the actual alignments.
%
First, we need to generate our reference
(either a transcriptome or genome)
so that the program has something to align against.
%\
This should only take a couple minutes,
so we could run it directly from the command line.
%
Move into the \verb;seqData; directory.
% , make a directory named `ref' if it doesn't exist, and 
% \emph{one} of us will run the following.
%
Please, do not run this in the workshop unless told to --
having multiple iterations of this run simultaneously
leads to corrupted files.


\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Only one student should run this:}}
cd \dataDirectory{}/seqData/transcriptome
rsem-prepare-reference --transcript-to-gene-map isotig_gene_map.txt \textbackslash
                       --no-polyA  reference.fasta gcatRef
\end{alltt}\end{kframe}\end{knitrout}

The files `reference.fasta' and `isotig\_gene\_map.txt',
contain the transcriptome in FASTA format and the list of
isoform to gene mappings.
%
The `gene\_map' tells the program which transcripts belong to the same gene,
which will allow us to analyze splice variants.
%
The flag `\verb;--;no-polyA' tells the program not to append poly-A tails to the transcripts.
%
This is used because some of these transcripts may be partial,
and should not include such tails.
%
Finally, the last input 'gcatRef' tells the program where to write this file.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Run the alignment} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sect:MappingRun}

The alignment we are going to run today will take several hours to complete
(possibly days) when running on full size files.
%
None of you likely wants to sit here with your computer connected for that long,
despite how much you love this class.
%
In addition, the some system will only allow jobs submitted from the command line
to run for a set amount of time
(in the case of Mason, 20 minutes)
before it aborts them.
%
This is done, in part, to make sure that large jobs are balanced on the machine.
%
So, today we will introduce a new method for running commands on a computer cluster:
a script and \verb;qsub;.
%
A script is just a series of commands to be executed
(a simple computer program),
and \verb;qsub; is the system that is used to control the running of those jobs.


Remember that we are \sout{lazy} efficient;
so we are going to start by copying and modifying a sample script.
%
Copy the file `sampleScript.sh' from the project directory
to your home directory.
%
Run \verb;less; to see what is in the file.

\begin{knitrout}\begin{kframe}\begin{alltt}
cp \dataDirectory{}/sampleScript.sh \mytilde{}/\hlnum{myFileName}.sh
cd \mytilde{}
less \hlnum{myFileName}.sh

\end{alltt}\end{kframe}\end{knitrout}


It should look something like this:


\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Contents of file ``sampleScript.sh'':}}

  # @ job_type = serial
  # @ class = NORMAL
  # @ account_no = NONE
  #PBS -m e -l vmem=10gb,walltime=1:00:00
  # @ notification = always
  # @ output = batch.\$(cluster).out
  # @ error = batch.\$(Cluster).err
  # @ queue
\end{alltt}\end{kframe}\end{knitrout}

The top three lines give information on how the job should be run and charged,
and aren't anything we need to worry about.
%
The line with `PBS' give several commands to the qsub program,
including to email you when the job is done (the -m e),
how much RAM to let you use,
and how long to let the job run.
%
The \verb;vmem; and \verb;walltime; commands are required,
as this is what the system uses to balance the jobs that are submitted.
%
The rest of the information tells the system how to save your outputs,
and that the job should go to the queue.

Below this, we are going to insert our commands.
%
These can be anything that you would submit in the terminal,
and the same syntax and rules apply,
including spelling and capitalization, so be careful.
%
To add these commands, we are going to use a word processing program: `nano'.
%
Run the command `\verb;nano;' followed by the name of your script.
%
It will open the document for editing in a simple editor.

\begin{knitrout}\begin{kframe}\begin{alltt}
nano \mytilde{}/\hlnum{myFileName}.sh
\end{alltt}\end{kframe}\end{knitrout}


The instructions at the bottom of the screen
tell you how to run the program,
but it is similar to notepad,
just without the click-able menus.
%
To save a file, type `\verb;Ctrl+o;', name the file
(or accept the current name) and hit enter.
%
To close the program, type `\verb;Ctrl+x;'.
%
Move to the bottom of the file
(the mouse won't work)
and add the following commands:

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Add this to your script using: nano \mytilde{}/myFileName.sh}}

cd \dataDirectory{}/seqData

rsem-calculate-expression --phred64-quals \hlnum{file}.fq \textbackslash
                          transcriptome/gcatRef align/\hlnum{file}

\end{alltt}\end{kframe}\end{knitrout}

The second line (wrapped above) calls the aligner,
and runs the expression calculation.
%
First, we tell it that the file has an offset of 64.
%
Next, we give it the name of our file to analyze;
do not forget to change the purple ``file'' portion to match your file name.
%
We then tell it which reference to use (the one we created above),
and finally the output name, which includes the directory
where we want to save it, and the name of your assigned sample.
%
%
The directory `align' should already be created
to ensure that your outputs are saved in the correct place
(create it using \verb;mkdir; if it doesn't exist).
%
Save the file (\verb;Ctrl+o;) and exit (\verb;Ctrl+x;).


You will notice that this line only works on one sample at a time.
%
If you want to run this for multiple files,
you will need to write a line like the above for each of your samples.
%
Once you are more comfortable with the Linux syntax, 
you may also want to explore GNU Parallel.
%
However, that syntax is beyond the scope of this tutorial.




Now, we will submit the job with this command:

\begin{knitrout}\begin{kframe}\begin{alltt}
qsub \mytilde{}/\hlnum{myFileName}.sh
\end{alltt}\end{kframe}\end{knitrout}

and now we wait.
%
We can see the progress (if any),
by calling `\verb;qstat;' which shows all running and queued jobs.
%
If your run:

\begin{knitrout}\begin{kframe}\begin{alltt}
qstat -u \hlnum{userName}
\end{alltt}\end{kframe}\end{knitrout}

You will see only your submitted jobs.
%
Run \verb;ls; in your home directory to see if there are any output files yet,
and use \verb;less; to view them.
%
They should be similar to the outputs you would expect to see in the terminal.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Many computer systems use a very similar job manager,
and so a search for most of the keywords we have used in this chapter
may help solve the problem that you are facing.
%
In addition, the man pages for `\verb;qsub;' and `\verb;qstat;' contain a great deal
of information on the submission of jobs.
Finally, the `Further Reading' includes the websites for each of the programs we used today,
and they have extended discussions of many available options.



% \needspace{6em}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

More information related to these topics can be found in:
\begin{itemize}
 \item \url{http://tophat.cbcb.umd.edu/manual.shtml}
 \item \url{http://bowtie-bio.sourceforge.net/index.shtml}
 \item \url{http://bowtie-bio.sourceforge.net/manual.shtml} 
 \item \url{http://cufflinks.cbcb.umd.edu/}
 \item \url{http://deweylab.biostat.wisc.edu/rsem/}
 \item \url{http://deweylab.biostat.wisc.edu/rsem/README.html}
\end{itemize}

% 
% \begin{knitrout}\begin{kframe}\begin{alltt}
% code here
% \end{alltt}\end{kframe}\end{knitrout}
