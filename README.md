Tutorial for RNAseq and R
=========================

These tutorials are written and maintained by
[Mark Peterson](mark.phillip.peterson@gmail.com).
They were originally written for teaching at Juniata College,
and adapted for summer workshops from
[GCAT-SEEK](http://lycofs01.lycoming.edu/~gcat-seek/workshops.html)
at Juniata and Lycoming Colleges in summer 2013 and 2014.
These tutorials are currently submitted to CourseSource for publication,
so please check back for a full citation to use if you publish with these.


Updated versions of both tutorials are available in the
[downloads](https://bitbucket.org/petersmp/workshopmodules/downloads),
along with the simple data needed for the R tutorial.
The sample data for the RNAseq tutorial will be hosted at CourseSource;
to access it before final publication, please contact the maintainer.


Directions for preparing these files are in the CourseSource publication.
To download the full contents of the repo,
e.g., in order to modify and remake the files,
you can select "Download Repository" from the 
[downloads](https://bitbucket.org/petersmp/workshopmodules/downloads).
Alternatively, if you would like to continue using git to track changes,
and submit those updates back to us, 
use the "clone" option from actions in the left hand panel
to copy the git command for cloning
(feel free to ignore this if you know nothing about git).



Repository Contents
===================

- **images**
	- contains the image files for all of the repos

- **Rtutorial** 
	- Contains the basic R tutorial
	- The source Rnw files; though these are deprecated
	- The LaTeX output from those Rnw files, heavily editted
	- A main LaTeX document, *learningR.tex*,
	  on which pdflatex should be run
	- A **data** directory, with the data used in the module.
	  A zipped version of this is available in Downloads.
	- A set of current and recent versions of the generated pdf
	  are available in the Downloads area.

- **rnaSeqAnalysis** 
	- Contains the main module
	- The LaTeX documents for each chapter,
	plus a sample layout to get you started if you want to add a chapter.
	- The main LaTeX document, *sequenceAnalysis.tex*
	on which pdflatex should be run
	- A set of current and recent versions of the generated pdf
	are available in the Downloads area.

- **sharedFiles**
	- Contains the shared style files for the two tutorials.

- **parallelScripts**
	- Contains the paralell scripts needed to run all of the data at once.
	- Additional installation and usage information is included within the
	*README* file within the directory.
