## Makes the reference; can easily be run without qsub

cd $dataLocation/seqData/transcriptome

rsem-prepare-reference --transcript-to-gene-map isotig_gene_map.txt \
--no-polyA reference.fasta gcatRef

