Information on full scripts
===========================

This directory contains scripts that can quickly run
the full pipeline described in the accompanying manual.
With relatively minor tweaks, they can also be used to
run a complete RNAseq analysis on full data.
Several of the scripts will need to be converted to
qsub scripts, you will need to change the names of reference files,
and you will need to ensure that your data files match the options
given (e.g., are single end, phred64, end in .fq).
This means you will likely need to edit the scripts directly
to match your files.
In addition, the time limits on these will not be sufficient for
a more complete analysis.
Make sure sure to change those options in the qsub scripts as well.


To run these scripts, you will need to install gnu parallel,
to do so, I used:

	(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - http://pi.dk/3) | bash

Following basic advice from <https://www.biostars.org/p/63816/>
which links to the full site of <http://git.savannah.gnu.org/cgit/parallel.git/tree/README>.
Make sure that it is included in your path before proceeding.


In addition, you will need to make sure you have installed the following:

- FastQC
- Java
- RSEM
- Bowtie
- Samtools
- VarScan


See the manual for more information on these installs.


You will then need to run the following scripts
using qsub for the ".qsub" files or directly for the ".sh" files.
For the larger ".sh" jobs, I have included qsub information at the top,
so they can be run through qsub if you would prefer.
For example:

	qsub bigScript.qsub
	./smallScript.sh

For each script, you will need to make sure the the directory information
has been set.
Please run the following lines, at the terminal, before running any of the scripts.
Make sure to change the location to match your actual directory,
which should be one directory above seqData.
Because it adds the variable to your .bashrc file as well, it only needs to be run once.


	export dataLocation=/N/dc2/projects/GCAT/workshopNewData/
	echo 'export dataLocation=/N/dc2/projects/GCAT/workshopNewData/' >> ~/.bashrc


These five scripts, in order,
will take you through the full module,
creating all needed files for other downloads.
If you want, you can skip runAllFastQC.sh or varScanFull.qsub,
as those are not relied on by other steps.


- **runAllFastQC.sh**
	- Runs quality control on all sample data files
- **makeReference.sh**
	- Converts the FASTA file into a usable reference
- **allAlignments.qsub**
	- Aligns all of the sample data sets
- **moveGeneResults.sh**
	- Move the gene result files to a new directory
- **varScanFull.qsub**
	- Conducts the full varscan analysis

