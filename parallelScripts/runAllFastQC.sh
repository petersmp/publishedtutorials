# @ job_type = serial
# @ class = NORMAL
# @ account_no = NONE
#PBS -m e -l vmem=10gb,walltime=0:30:00,nodes=2:ppn=4
# @ notification = always
# @ output = batch.$(cluster).out
# @ error = batch.$(Cluster).err
# @ queue


## Run fastqc on all files

cd $dataLocation/seqData

parallel fastqc -o fastQCresults fastqc {} ::: *.fq

