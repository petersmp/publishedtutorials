%% Plotting in R
\documentclass[11pt]{report} %% May want to try twocolumn soon
\usepackage{myStyle}
\ProvidesPackage{myStyle}

%% Delete for book
\title{Statistical Tests in R}
\setcounter{chapter}{1} % Set to 1 less than the desired number

\author{Dr. Mark Peterson}
\date{}

\begin{document}


%% Keep from here down
\chapter[Statistical Tests in R]{Statistical Tests in R}

<<setup, include=FALSE, echo=FALSE, cache=FALSE,results="hide",warning=FALSE>>=
  library(knitr)
  library(gplots) 
#  library(lme4) # Only needed if running lmer
# set global chunk options
opts_chunk$set(fig.path='figure/statTests-', fig.align='center', 
               fig.show='hold', cache=FALSE, tidy=FALSE)
options(replace.assign=TRUE,width=90)
hook_source = knit_hooks$get('source')
knit_hooks$set(source = function(x, options) {
  txt = hook_source(x, options)
  # extend the default source hook
  gsub('~', '\\\\mytilde', txt)
})

setwd("~/Documents/Juniata/teaching/eukGenomics/lectures/labs")

## Read in all data here:
fastPlant <- read.csv("data/fastPlant.csv")


hotdogs <- read.csv('data/hotdogs.csv')

violations <- read.csv("data/violations.csv")
## Set the levels of Year to the normal order
violations$Year <- factor(violations$Year,c('Fr','So','Jr','Sr'))
## Save and display a more useful table
violationsTable <- table(violations$Fine,violations$Year)

scaleSqueeze <- read.csv("data/scaleSqueeze.csv")

@

\section{Background}
Hypothesis testing lies at the heart of statistics.
%
Whether it is determining whether or not two samples differ,
whether or not a mean is different than zero,
or testing the effect of many variables simultaneously,
statistical tests help us to understand the world around us.
%
All of the tests you will use in class were developed by hand
and, for much of their time, have been calculated by hand.
%
% You have worked through several examples of this before,
% but 
Today we will be learning how to use a computer program
to compute the statistics for us.

\subsection{Functions and test in R}
We will again be using the R statistical environment and
RStudio to conduct these analyses.
%
The community based nature of R ensures that all of the common
statistical tests are available to us and that there is extensive
help and documentation available online.
%
For our statistical tests today,
we will be calling built in functions in R, using the syntax:
``functionName( argument1=value, argument2=value, \ldots)''
to generate a result.
%
The help ``?functionName'' is great for these tests,
but is sometimes not clear on how a function is actually being calculated.
%
% To address this, we will cover, as a lecture,
% the implementation of the ks.test() function
% as an example of what is occuring ``under the hood.''
% %
% You may wish to follow along in RStudio,
% but the lecture slides and a handout will be provided
% for your later reference.


\subsection{Updating an old script}
Today, you will be relying (rather heavily) on the script
that you generated in the last chapter.
%
Refer to your notes (comments) in the script to help you,
and remember how grateful you are for those as you comment today.
%
Focus on writing your scripts well the first time,
and you will save yourself \emph{massive} amounts of time in the future.
%

\subsection{Reminders}

Below are a few notes to remember from the last chapter.
%
For more details, refer back to the previous manual
or to your notes in the old script.
\begin{itemize}
  \item{Comment and save frequently.}
  \item{Highlighted commands, or the current line, 
        are sent from your script to the console
        with either Ctrl+Enter, or by clicking ``run.''}
  \item{Run every line of your script, including comments,
        as you add them. This makes catching errors much easier,
        since you will know exactly which line contains the error.}
  \item{Always set your working directory near the top of your script.}
  \item{Reading in data works relative to your working directory,
        which may help you identify errors.}
\end{itemize}


\section{What you will learn today}
\begin{itemize}
  \item How to run basic statistical tests
  \item How to utilize previous scripts to save time
  \item The basic syntax needed for \emph{lots} of genomics tests
\end{itemize}


\section{Getting Started}
\subsection{What you need for today}
Today, you will need:
\begin{itemize}
  \item Your old script
  \item Your computer with R and RStudio installed (see previous chapter for details)
  \item The ``data'' folder from the last chapter
\end{itemize}

Make sure that you have all of these available before we start.
%
Refer back to your old code and lab manual frequntly for more information
on topics we covered before.

\subsection{Setting up your script(s)}
First, open RStudio and open your previous script from ``File $\rightarrow $ Open File \ldots''
%
Then, select ``File $\rightarrow $ New $\rightarrow $ R Script'' to open a new document.
%
This document will hold all of our commands for today's lesson,
so save it with a meaningful name, in the same directory as your old script.
%
Make sure that this name includes your last name
along with something to distinguish it from the previous script.
%
As we did last time, you will be turning in
the html output we create from the script at the end of the lab.


Now, lets add some information to the top of the script.
%
From your old script, copy over the header information;
modify it with today's date and description.
%
Remember that, in R, anything that is preceded by a ``\#'' is ignored by the console,
which allows us to add \emph{very} useful comments to ourselves.
%
It should look something like this:

<< tidy=FALSE,results='hide',warning=FALSE>>=
## Script written by FIRSTNAME LASTNAME
## Euk Genomics R lab 2- learning tests
## 2014 Jan 27
@

These comments will help you when, like today, 
you go back to old code for help.
%
Make sure to add comments throughout your script today,
particularly to answer questions posed in the manual.
%
You will be graded on these answers, so make sure to be clear in the script.


Next, set your working directory.
%
Go to your old script and copy the ``setwd()'' command,
it should like something like:

% NOTE: This will not change my working directory, need to include data
% in the folder with the LaTeX and .rnw files
<< ,results='hide',warning=FALSE,echo=TRUE, tidy=FALSE,eval=FALSE>>=
## Manually set working directory
## Make sure that this is set for your computer, not mine
setwd('~/Documents/eukGenomics/scripts/')
@




\section{The t-test}

One of the most common statistical test is the ``t-test.'' 
%
By now, you have likely calculated t-tests by hand in other classes.
%
For larger data sets, however, you may find such hand calculations cumbersome.
%
Luckily, R has a built in function to calculate t-tests for us.
%
Take a look at the help for the t-test in R using ``?t.test''
and see what options are available.


To see what the output of a t-test looks like,
let's try running it with a set of random data.
%
This approach allows us to control a number of factors,
including the difference between samples and the standard deviation.
%
Use the following code to generate two random variables,
then run a t.test to compare them.
%
If you are unsure of the options in ``rnorm()'' use the help,
or hit tab to see what is available.

<< ,results='hide',warning=FALSE,eval=TRUE,echo=FALSE>>=
## set the seed to ensure reproducible test
set.seed(16652)
@

<< ,results='hide',warning=FALSE,eval=TRUE,echo=TRUE>>=
## Generate two random samples with different means
randA <- rnorm(n=20,mean=10,sd=5)
randB <- rnorm(n=20,mean=15,sd=5)

## Run a test to compare them
t.test(randA,randB)
@

This output shows a lot of information,
each bit of which may be useful for different applications.
%
The line beginning with ``t='' gives us all of the test statistics,
including the p-value.
%
Here, we can see that the two samples are indeed different,
as we expected them to be.
%
Now, go back and change the mean of randB to 10
and rerun the t-test.
%
Note the change in p-value, your result is likely not significant any more.
%
If it is, compare with your neighbors and consider why about 1 of the 20
students in the class may get a significant result.


\subsection{Power and the t-test}

<< tidy=FALSE,results="hide",warning=FALSE,echo=FALSE>>=
fastPlant <- read.csv("data/fastPlant.csv")
@

Now, let's try running the t-test with some of the data we used in the last lab,
starting with the fastPlant data.
%
Go to your previous script and copy over the lines to
read in the fastPlant data.
%
Then, add the code for the final plot (using plotmeans)
to give you a visual guide to these data.
%
Don't forget that you will need to load the gplots library
for this plot to display.


Because there are two grouping variables, we will focus on just one subset:
the plants in low-light.
%
Later in this lesson, 
we will explore how to analyze more of this data at once.
%
The t.test function provides a simple way 
to limit the data analyzed with the ``subset'' argument.
%
The subset option works with the ``formula'' approach to the t.test,
which is similar to the formulas we have used in the past.
%
Note that we can either include ``fastPlant\$'' before each variable,
or include ``data=fastPlant'' in the function call.
%
Many, but not all, functions include this option,
and it can be a great time saver and make the outputs a little cleaner.
%
The code to run this test is either:
<< ,results='hide',warning=FALSE,echo=TRUE>>=
## Run a t test on a portion of the fastPlant data
t.test(fastPlant$Height ~ fastPlant$Fert,subset= (fastPlant$Light=="Low") )
## Or
t.test(Height ~ Fert,data=fastPlant, subset= (Light=="Low") )
@

Based on this output: does fertilizer amount affect plant growth
when lighting is low?
%
Add a comment to your script about this.
%
However, one concern we have about this analysis is that
power can often be an issue for t-tests,
which may make you reconsider your conclusion.
%
To see how much confidence we should place in this finding,
we'll utilize R's built in power test: ``power.t.test()''
%
Open the help for the power function to see the details
of which options we need to include.
%
For this test, we need to supply 4 of these 5 parameters:
  \begin{itemize}
    \item{n= sample size \emph{per group}}
    \item{delta= the difference between the groups}
    \item{sd= the standard deviation of the data}
    \item{sig.level= the significance level desired, and}
    \item{power= the portion of tests in which a significant result will occur}
  \end{itemize}  

\ldots and R will calculate the fifth parameter.
%
First, we are interested in the power we have to detect an effect of fertilizer,
assuming that our data are an accurate reflection of the real population.
%
We could calculate each of the values directly,
but we have already generated some of them.
%
Our plotmeans() graph tells us how many samples are in each group.
%
The t.test() output gives us the two means,
from which we can calculate the difference.
%
However, we need to calculate the standard deviation,
which we can calculate in R using the sd() function.
%
Add the numbers you found  in place of the X's below:

<< ,warning=FALSE,results='hide',echo=TRUE,eval=FALSE>>=
## Calculate the standard deviation
sd(fastPlant$Height[fastPlant$Light=="Low"])

#Calculate the power of the T test
power.t.test(n=X,delta=X,sd=X,sig.level=0.05)
@
%plotmeans(Height ~ interaction(Fert,Light,sep='\n'), connect=FALSE,data=fastPlant)
%power.t.test(n=24,delta=2,sd=4.34,sig.level=0.05)

Based on this, what proportion of the time would we have been
able to identify a significant result?
%
Does this change your confidence in the p-value you calculated above?
%
Add comments to your script addressing these questions.


Next, we can calculate the number of samples we would need to collect,
given the data, to get a significant result 90\% of the time.
%
Copy your power test to a new line, and remove the ``n='' option,
then add the option ``power=0.9'' to the command.
%
This tells R to calculate the n (now missing) necessary to achieve
a significant result in 90\% of the experiments testing this.
%power.t.test(delta=2,sd=4.34,sig.level=0.05,power=0.9)
%
How many samples would you need to collect to
reliably detect your difference?
%
This is an important point to remember when evaluating
the output of statistical tests.


\subsection{Normality and the t-test}
T-tests rely on the assumption that 
the data being analyzed are normally distributed.
%
When this assumption is violated,
the results of the test may not be informative.
%
In this section, we will use the hotdog data to illustrate this point.
%
First, copy (from your old script)
the lines that read and attach the hotdog data.
%
Then, copy over and run your final plot (with colored points)
to help you visualize the data.
<< ,results="hide",warning=FALSE,echo=FALSE,fig.show='hide'>>=
hotdogs <- read.csv("data/hotdogs.csv")

plot(hotdogs$Calories, hotdogs$Sodium,main="hotdogs",)
points(hotdogs$Calories[hotdogs$Type=='Beef'],hotdogs$Sodium[hotdogs$Type=='Beef'], pch=19,col='red')
points(hotdogs$Calories[hotdogs$Type=='Poultry'],hotdogs$Sodium[hotdogs$Type=='Poultry'], pch=19,col='yellow')
points(hotdogs$Calories[hotdogs$Type=='Meat'],hotdogs$Sodium[hotdogs$Type=='Meat'], pch=19,col='blue')
legend(x=170,y=350,legend=c('Beef','Poultry','Meat'),
       fill=c('red', 'yellow','blue'),title='Type')
@

Now, lets see if there is a significant difference in Sodium
between the Poultry and Beef hotdogs.
%
We can use the formula input of t.test() again, 
though note that we need to omit the data we are not analyzing.
%
Because the t-test compares two groups,
R will have an error if all three Types are included.
%
Once again, a later test in this lesson will
allow us to analyze all of the groups simultaneously.
%
The command should look like this:
<< ,warning=FALSE,results='hide',echo=TRUE>>=
## Run a t-test on sodium
t.test(Sodium~Type,subset= (Type!="Meat"),data=hotdogs )
@

Based on this result, is there a significant difference?
%
Include a comment in your script here.
%
It is possible that power is again an issue here,
but there is actually another possible culprit: normality.
%
Looking at the Poultry dots,
do the Sodium values appear normally distributed?
%
We can look at this more formally with a series of plots and tests.
%
First, lets look at a histogram of the Poultry Sodium data:
<< results="hide",warning=FALSE,echo=TRUE,fig.show='hide'>>=
## Make a histogram of the Poultry sodium
hist(hotdogs$Sodium[hotdogs$Type=="Poultry"],)
@

Does this look normally distributed?
%
Next, let make a QQ-plot for the same data.
%
Refer to the help or the previous manual for more
information on the qqnorm() commands.
<< results="hide",warning=FALSE,echo=TRUE,fig.show='hide'>>=
## Make a QQ-plot and add a line
qqnorm(hotdogs$Sodium[hotdogs$Type=="Poultry"],datax=TRUE)
qqline(hotdogs$Sodium[hotdogs$Type=="Poultry"],datax=TRUE,col='red')
@
What can you see now?
%
Finally, let's run a pair of normality tests on the data:
<< results="hide",warning=FALSE,echo=TRUE,fig.show='hide'>>=
## Test normality
ks.test(scale(hotdogs$Sodium[hotdogs$Type=="Poultry"]),pnorm)
## Use a different test to compare:
shapiro.test(hotdogs$Sodium[hotdogs$Type=="Poultry"])
@

Note here the rather substantial differences in these two tests.
%
The ks.test(), which we covered earlier,
is sensitive to large outliers in the data.
%
In contrast, the Shapiro-Wilk test is sensitive
to small deviations from normality across the distribution.
%
It is important to remember that statistical tests can have such biases,
and can lead to very different conclusions.
%
Summarize your conclusions in comments to the script.


Now that we suspect that the data are not normally distributed,
we should likely use a non-parametric test.
%
Remember from lecture that non-parametric tests often utilize
rank-order data instead of raw values.
%
This makes them less sensitive to the distribution of the data,
and a good choice for non-normal data.
%
One of the non-parametric equivalents of the t-test is the
Mann-Whitney-Wilcoxon test, available in R as wilcox.test().
%
This test runs very similarly to the t.test,
so copy your t.test() line and change ``t.test'' to ``wilcox.test''.
%
Run the test, and interpret the result as a comment in your script.
<< ,warning=FALSE,results='hide',echo=TRUE>>=
## Run a non-parametric test on sodium
wilcox.test(Sodium~Type,subset= (Type!="Meat"),data=hotdogs )
@

It is important to note that non-normal data can cause spurious
results in both directions.
%
False negatives, like we saw here, 
or false positives, 
which would incorrectly reject the null-hypothesis.
%
When running statistical tests,
it is a good idea to check your assumptions \emph{before}
running the tests to avoid erroneous conclusions.

\section{Chi-squared Test}
<< ,results="hide",warning=FALSE,echo=FALSE,fig.show='hide'>>=
violations <- read.csv("data/violations.csv")

## Set the levels of Year to the normal order
violations$Year <- factor(violations$Year,c('Fr','So','Jr','Sr'))

## Save and display a more useful table
violationsTable <- table(violations$Fine,violations$Year)
violationsTable
@
Another useful statistical test is the chi-squared test,
available in R as ``chisq.test()''.
%
As you have covered in lecture,
the chi-squared test is for independence of data.
%
This is usually, though not always,
count data, similar to the violationsData set from last lab.
%
From your old script, copy the lines to import the violations data,
attach it, change the variable order, and make the violationsTable.

Looking at the violationsTable,
we can see that the high fine levels are very rare.
%
Such low values cause problems for the chi-squared test,
so first lets see what happens when we run a chisq.test()
on the first two rows of the table:
<< ,results="hide",warning=FALSE,echo=TRUE,fig.show='hide'>>=
## chi-squared test on low level fines
chisq.test(violationsTable[1:2,])
@
What does this result suggest?
%
Add a comment to your script interpreting the table.
%
Now, copy the line and re-run the test with the full table,
and look at the resulting warning:
<< ,results="hide",warning=TRUE,echo=TRUE,eval=TRUE,fig.show='hide'>>=
chisq.test(violationsTable)
@
Note that the approximation may not be correct,
in this case because the sample sizes are too small
in some of the cells.
%
Taken with our previous result,
this may suggest that a larger sample may reveal
a significant relationship between Year and Fine amount
at all levels.


\section{ANOVA}
Finally, today we will cover another important statistical test:
Analysis of variance, typically called ANOVA.
%
This test allows us to compare several groups at once,
and to analyze more than one variable.
%
There are several flavors of ANOVA that you may encounter;
today, we will work through how to run three of them in R:
One-way, random blocks, and two-way ANOVA.
% ADD REFERENCES TO THE LOCATIONS
%
Refer to your lecture notes and handouts for more detail on the
interpretation and structure of each of these tests.

\subsection{One-way ANOVA}
The simplest kind of ANOVA is the one-way ANOVA,
which compares the means of groups separated by
a single factor.
%
In principle, any number of groups can be analyzed;
however, for only two groups, the test is equivalent
to the t-test.
%
To illustrate this, lets analyze some of the hotdog data,
specifically: do different types of hotdog have different calories?
%
Make sure the hotdog data is loaded
and pull up the plot to help you visualize as you go.
%
Now, let's run an ANOVA using the built-in R function aov():
<< ,results="hide",warning=TRUE,echo=TRUE,fig.show='hide'>>=
## Run an ANOVA
aov(Calories ~ Type,data=hotdogs)
@

This gives us much of the information that we need,
though not all of it.
%
%====================================================
%======= UNBALANCED INFO, STRIKE IF POSSIBLE ========
%====================================================
Importantly, it also provides us with a warning and
a reminder: these data are unbalanced, violating one
of the assumptions of ANOVA.
%
For a one-way ANOVA, this is not a major problem,
but it can be a concern for more complicated designs.
% NB: anova( glm(Formula), test="F")
% Gives correct stats, but order of Formula matters
%===================================================
%======= UNBALANCED INFO, STRIKE IF POSSIBLE =======
%===================================================
%
What we would really like from R, however,
is an F-statistic, and a calculated p-value.
%
Each could be calculated by hand from this output,
but that seems like work.
%
Luckily for us \sout{lazy} efficient programmers,
R already has a method to do this for us: summary().
%
The function summary() works differently on different classes
of data, but for most statistical tests,
it will output the sort of final information you are likely to want.
%
So, first we will save the result of the aov() call,
which works just like other variables, 
and can store the results of most R functions.

<< ,results="hold",warning=TRUE,echo=TRUE,eval=TRUE,fig.show='hide'>>=
## Run and save an ANOVA
anovaResult <- aov(Calories ~ Type,data=hotdogs)
## Display the results
summary(anovaResult)
@

What does this result suggest about the relationship
between Type and Calories?
%
Knowing whether or not there is an effect of Type on Calories
still doesn't tell us \textit{what} that effect is.
%
Below, we use two R functions to show more detailed information.
%
The model.tables() function calculates the effects or means of each group,
while the function TukeyHSD() computes the post-hoc Tukey test
and the adjusted p-values for multiple-testing.
%
Use the R code below to run these functions
then, add comments to your code describing the results.

<< ,results="hide",warning=TRUE,echo=TRUE,fig.show='hide'>>=
## Generate the table of effects
model.tables(anovaResult)

## Generate a table of means instead (more intuitive)
model.tables(anovaResult,type="means")

## Run a statistical test to determine which groups differ
TukeyHSD(anovaResult)
@

Note the relationship between these outputs.
%
The model.tables() function outputs the effect sizes,
which can be interpreted as the difference between the
group mean and the grand mean.
%
Adding the ``type='means' '' portion gives you the actual group means,
along with the calculated grand mean.
%
Finally, TukeyHSD() yields the confidence intervals for the difference
between each pair of groups, adjusted for multiple testing.
%
That is, the output of TukeyHSD() tells you the difference in the means,
the lower and upper limit of the confidence interval, and a p-value
for the probability of being that much (or more) different from zero,
given the null hypothesis.
%
Each of these outputs provide a useful insight into the data,
and will be used again in the coming sections.


\subsection{Two-way ANOVA}
Next, we are going to briefly introduce the two-way factorial ANOVA.
%
% This, and its interpretation,
% will be covered in more depth in a later lecture.
% %
% For now, we will simply generate the output for you reference.
% 
% 
Plot the full plotmeans() plot for your fastPlant data again.
%
What do you notice about the effects of Light and Fert
in relation to each other?
%
We can use the interaction plot function to make this even more clear:

<< ,results="hide",warning=TRUE,echo=TRUE,eval=TRUE,fig.show='hide'>>=
## Make an interaction plot for the fastPlant data
interaction.plot(x.factor=fastPlant$Fert, trace.factor=fastPlant$Light, 
                 response=fastPlant$Height, main="TITLE")
@

It appears that the two treatments may be affecting one another,
but we would like to know more about exactly how.
%
First, let's run an ANOVA describing the effects of both
treatments on Height.
%
The syntax is similar to the randomized blocks ANOVA,
but we are interested in analyzing the second variable,
not just accounting for the random variance.

<< ,results="hide",warning=TRUE,echo=TRUE,fig.show='hide'>>=
plantAnova <- aov(Height ~ Light + Fert,data=fastPlant)
summary(plantAnova)
@
What does this finding suggest?
%
However, there is still more that we can analyze,
particularly the interaction between the two treatments.
%
To do this, we replace the ``+'' with an asterisks (*) 
to tell R to analyze the interaction between the two, 
like this:

<< ,results="hide",warning=TRUE,echo=TRUE,fig.show='hide'>>=
plantAnovaInt <- aov(Height ~ Light * Fert,data=fastPlant)
summary(plantAnovaInt)
@
% The details of how to interpret this output will be covered
% later in the semester.

\subsection{Using linear models}
In R, we have been calling the aov() function directly;
however, R is internally using linear models to run these tests,
and these linear models can be accessed directly.
%
This can be especially important for analyzing data that are linear,
instead of grouped neatly,
including when there are random factors,
such as individual,
that can add information to our analysis.
%
We have one such data set in the scale squeezing data collected
in the biostats class.
%
Each student pressed on a bathroom scale with their dominant arm, 
with their elbow resting on a book so their arm was parallel 
to the surface of the scale.
%
Scale readings recorded in 5-second intervals, 
with the first reading taken once the scale initially reaches a maximum. 


We will analyze these data looking at the effect of fatigue
on pressing, taking into account the differences in who was pressing.
%
Load in the data set, either using read.csv(),
modified for your computer, or the menus,
but don't forget to paste the command into your script.
%
It is good practice to look at
what is in the dataset to ensure it read properly.

<< ,results="hide",warning=TRUE,echo=TRUE,fig.show='hide'>>=
## Read in the data
scaleSqueeze <- read.csv("data/scaleSqueeze.csv")

## See what is there
head(scaleSqueeze)
@

Now plot the data:
<< ,results="hide",warning=TRUE,echo=TRUE,fig.show='hide'>>=
plot(scaleSqueeze$pounds ~ scaleSqueeze$time, main="TITLE")
@

Look at the data and add a comment to your script
describing the pattern(s) you see.
%
From this, we might be interested in an ANOVA to tell us about
the effect of time (fatigue) on the strength of the press.
%
Run (and save and display) a simple ANOVA doing this with the command:
<< ,results="hide",warning=TRUE,echo=TRUE,eval=TRUE,fig.show='hide'>>=
## Anova test and output
scaleAnova <- aov(pounds ~ time,data=scaleSqueeze)
summary(scaleAnova)
@
What conclusion can you draw from this test?
%
What does it suggest is happening,
and does this match your previous prediction?

However, this only tells us \textit{that} time has an effect,
not what that effect is.
%
For that, we can use linear models via the lm() function.
%
This function constructs a linear model, 
giving us both intercept and the effect of variables.
%
Let's look at this and compare it to our aov() output.

<< ,results="hide",warning=TRUE,echo=TRUE,eval=TRUE,fig.show='hide'>>=
## Anova test and output
scaleLM <- lm(pounds ~ time,data=scaleSqueeze)
summary(scaleLM)
anova(scaleLM)
summary(scaleAnova)
@

Note first that the summary() of the lm() output tells us
the effect of time on pushing:
it appears that each unit of time (seconds) reduces pressing by
\Sexpr{scaleLM$coefficients['time']} pounds.
%
Further, what do you notice about the p-values for the three outputs?
%
How about the differences between the two ways of getting an anova output?


The result looks fairly clear,
but we have additional information that we can use.
%
It seems like the pattern may be stronger even than
the test calculated,
but it may be hampered by the large amount of individual variation.
%
Luckily, we know which individual is measured at each time,
and we can give this information to the computer.

First, let's plot the data using the individual information
to give ourselves a better sense of what we have.
%
We could color code each individual,
like we did for the hotdog data by Type,
but that is a bit cumbersome, and we are
\sout{lazier} more efficient than that.
%
There is, as is usually the case, 
an R function that will do the work for us:
interaction.plot().
%
Use the help or tab for more details on this command,
but the below syntax should produce a nice plot for you:

<< ,results="hide",warning=TRUE,echo=TRUE,eval=TRUE,fig.show='hold',fig.width=8, fig.height=7, out.width='.4\\linewidth',tidy=FALSE>>=
## Add individual information to the plot
interaction.plot(x.factor=scaleSqueeze$time, trace.factor=scaleSqueeze$Ind,
                 response=scaleSqueeze$pounds, main="TITLE",
                 trace.label='LEGEND')
@

What information did this add?
%
Now, we want to add this same information into our lm() and anova() analyses.
%
%% NB: this is the 'RIGHT' way to do this, but makes other things way harder
% In R, this is implemented by the command Error() within aov.
% %
% Putting an Error() term into the formula tells R 
% that there is a random effect,
% in this case the individual, 
% and that it should remove that variation
% from the final analysis.
%
This syntax will run the analysis for you:

<< ,results="hide",warning=TRUE,echo=TRUE,fig.show='hide'>>=
scaleLMwithInd <- aov(pounds ~ time + Ind,data=scaleSqueeze )
summary(scaleLMwithInd)
@

What is the difference between the two ANOVA's?
%
Add a comment to your script to interpret your findings,
specifically the difference between the two test results.

% Below is the way that I would personally run this analysis.
% However, it is too much for students.
% Now that I made Ind a factor, the top one yields the same result
% It does not, however, when time is treated as an un-ordered factor
% Worth exploring later.
<< ,results="hide",warning=TRUE,echo=FALSE,fig.show='hide',eval=FALSE>>=
## MY preferred way
library(lme4)
randGLM <- lmer(pounds ~ time + (1|Ind),data=scaleSqueeze)
anova(randGLM)
plot(scaleSqueeze$pounds ~ scaleSqueeze$time)
summary(randGLM)
abline(summary(randGLM))

randGLM2 <- lmer(pounds ~ factor(time) + (1|Ind),data=scaleSqueeze)
anova(randGLM2)
summary(randGLM2)
@
 

\section{Assignment -- due before class Wednesday}

For your assignment today, you will need to turn in your script
(which you have been generating all along) 
and an html output from the script to confirm that your code worked.
%
You will follow the same steps, and need the same packages,
as last time.
%
Refer to the previous manual for more details.


Click on the small notebook icon just above your script. 
%
It should say ``Compile an HTML notebook from the current R script'' 
when you mouse over it. 
%
Assign an appropriate title, and put your name as the author. 
%
Click ``Compile'' and an html file will be created. 
%
Email both your script and the created html file to me 
(petersm@juniata.edu) by the end of the day Friday.

\end{document}

